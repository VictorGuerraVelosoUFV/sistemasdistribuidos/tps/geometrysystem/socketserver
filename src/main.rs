extern crate rand;

use serde_json::json;
use rand::{thread_rng, seq::SliceRandom, Rng};
use rand::prelude::ThreadRng;
use std::net::{TcpStream, TcpListener, Shutdown};
use std::io::{Read, Write};
use std::{str, thread};

fn handle_client(mut stream: TcpStream, shapes: [&str; 4]) {
    const BUFFER_SIZE: usize = 16;
    // read 20 bytes at a time from stream echoing back to stream
    println!("Client connected!");
    let mut read = [0; BUFFER_SIZE];;
    let js;
    let js_string;
    match stream.read_exact(&mut read) {
        Ok(_) => {
//                if n == 0 {
//                    // connection was closed
//                    break;
//                }
            let text_read = str::from_utf8(&read)
                .expect("Could not convert to str 1")
                .trim_matches(char::from(0));
            println!("Texto lido: {}",text_read);
            let text = if text_read == "GeometryGenerate" {
                js = genjson(shapes);
                js_string = serde_json::ser::to_string(&js).unwrap();
                js_string.as_bytes()
            } else { b"Error wrong command (Try:GeometryGenerate)" };

            let text = text;
            println!("Respondendo...");
            stream.write(text)
                .expect("Could not send response msg");
            stream.shutdown(Shutdown::Write)
                .expect("Could not shutdown");
        },
        Err(err) => {
            println!("Deu ruim");
        }
    }
    println!("Pronto!");

}
fn genjson(shapes: [&str; 4]) -> serde_json::value::Value {
    println!("Gerando JSON...");
    let mut rng: ThreadRng = thread_rng();
    let shape: &str = shapes.choose(&mut rng).unwrap();
    let mut x: [i32; 2] = [rng.gen_range(0, 768), rng.gen_range(0, 768)];
    let mut y: [i32; 2] = [rng.gen_range(0, 768), rng.gen_range(0, 768)];
    x.sort();
    y.sort();
    json!({
        "shape": shape,
        "width":x[1]-x[0],
        "height":y[1]-y[0],
        "pos":{
            "x":x[0],
            "y":y[0]
        }
    })
}

fn main() {
    let shapes: [&str; 4] = ["TRIANGLE", "SQUARE", "CIRCLE", "RECTANGLE"];
    let listener = TcpListener::bind("127.0.0.1:8080").unwrap();

    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                thread::spawn(move || {
                    handle_client(stream, shapes);
                });
            }
            Err(_) => {
                println!("Error");
            }
        }
    }
}
